/* sg.c - A small word count program
   Ayush Jha
   jha.ayush@mail.com
   */

#define _GNU_SOURCE 1

#include <hurd.h>
#include <hurd/fd.h>
#include <hurd/io.h>
#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <error.h>
#include <string.h>
#include <argp.h>

#define BUFLEN_READ 25 /* Buffer length to read at once with
                          io_read*/

int USE_STDIN = 0; /* read from stdin? */

/* defining lobal variables */
const char *argp_program_version = "shabda ginti 1.0";

const char *argp_program_bug_address = "<jha.ayush@mail.com>";

static char doc[] = 
"shabda giniti(sg) - a simple clone of the wc program";

static char args_doc[] = "<input file>";

/* this struct is used by main to communicate with parse_opt */
struct arguments{
  char *args[1];  /* ARG1 */
};



static error_t
parse_opt(int key, char *arg, struct argp_state *state)
{
  struct arguments *arguments = state->input;
  switch (key){
    case ARGP_KEY_ARG:
      if (state->arg_num >= 2)
        /* too many arguments */
        argp_usage(state);
      arguments->args[0] = arg;
      break;
    case ARGP_KEY_END:
      if (state->arg_num < 1)
        /* not enough arguments, read from stdin */
        USE_STDIN = 1;
      break;
    default:
      return ARGP_ERR_UNKNOWN;
  }
  return 0;
}

static struct argp argp_s = {0, parse_opt, args_doc, doc};

int main(int argc, char *argv[])
{
  struct arguments args_s;

  /* Create a file */
  file_t in; /* file_t is a typedef of mach_port_t.
                we use file_t to specify that
                we intend to use this port as a file*/

  struct hurd_fd *stdinfd =_hurd_fd_get(0); 
  /* get file descriptor for stdin i.e. 0, using _hurd_fd_get from
     hurd/fd.h hurd_fd contans a port attribute
     which is a struct `hurd_port' from hurd/port.h.
     hurd_port contains a port attribute with is a io_t type.
     io_t is a typedef of mach_port_type. This port can be used by
     io_read to read data from stdin. The file descriptor for
     stdin is always defined by 0.
  */

  argp_parse(&argp_s, argc, argv, 0, 0, &args_s);
  error_t err; /* stores the errors in the code;
                  used to check for errors */
  mach_msg_type_number_t amount; /* amount of data
                                    that was actually written
                                    by the server */
  if (USE_STDIN) /* if USE_STDIN is true, store the stdiin port
                    else, read the argument
                  */
    in = stdinfd->port.port;
  else
    in = file_name_lookup(args_s.args[0], O_READ, 0);
  if (in == MACH_PORT_NULL)
    error(1, errno, "Could not open file: %s", args_s.args[0]);
  char *buffer, *bufdup;
  buffer = malloc(BUFLEN_READ);
  bufdup = malloc(BUFLEN_READ);
  int in_len = 0;
  while (1)
  {
    err = io_read(in, &buffer, &amount, -1, BUFLEN_READ);
    if (err)
      error(1, errno, "Could not read from file: %s", args_s.args[0]);
    if (amount == 0)
      break;
    strcpy(bufdup, buffer);
    in_len += amount;
  }
  printf("%d %s\n", in_len, args_s.args[0]);
  return 0;
}
